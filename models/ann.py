#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan 24 14:30:32 2018

@author: i857753
"""
from sklearn.metrics import confusion_matrix
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import Dropout
from keras.wrappers.scikit_learn import KerasClassifier
from sklearn.model_selection import GridSearchCV
from sklearn.metrics import accuracy_score
import numpy as np


class NeuralNetwork:
    def __init__(self, input_layer_size, output_layer_size, hidden_layer_size=None):
        self.classifier = Sequential()
        self.grid_search_results = None
        self.accuracies = None
        self.input_layer_size = input_layer_size
        self.output_layer_size = output_layer_size
        self.hidden_layer_size = self.calculate_hidden_layer_size(hidden_layer_size, input_layer_size,
                                                                  output_layer_size)

    def build_neural_network(self, hidden_layer_quantity=1, hidden_layer_size=None, weights_value_init='uniform',
                             internal_activation_func='relu', output_activation_func='sigmoid', gradient_descent='adam',
                             error_function='binary_crossentropy', metrics=['accuracy'], dropout=0.1):

        hidden_layer_size = self.hidden_layer_size if hidden_layer_size is None else hidden_layer_size

        self.build_input_layer(hidden_layer_size, internal_activation_func, weights_value_init, dropout)

        self.build_hidden_layers(dropout, hidden_layer_quantity, hidden_layer_size, internal_activation_func, weights_value_init)

        self.build_output_layer(output_activation_func, weights_value_init)

        self.classifier.compile(optimizer=gradient_descent, loss=error_function, metrics=metrics)

        return self.classifier


    def build_input_layer(self, hidden_layer_size, internal_activation_func, weights_value_init, dropout):
        self.classifier.add(
            Dense(input_dim=self.input_layer_size,
                  activation=internal_activation_func,
                  units=hidden_layer_size,
                  kernel_initializer=weights_value_init
            )
        )
        self.classifier.add(Dropout(rate=dropout))

    def build_hidden_layers(self, dropout, hidden_layer_quantity, hidden_layer_size, internal_activation_func, weights_value_init):
        for hidden_layer in range(0, hidden_layer_quantity):
            self.classifier.add(
                Dense(units=hidden_layer_size, activation=internal_activation_func,
                      kernel_initializer=weights_value_init))
            self.classifier.add(Dropout(rate=dropout))

    def build_output_layer(self, output_activation_func, weights_value_init):
        self.classifier.add(
            Dense(units=self.output_layer_size, activation=output_activation_func,
                  kernel_initializer=weights_value_init))

    def run_parameter_tunning(self, data_training_set, class_training_set, parameters, callbacks=None, k_folds_number=10, number_of_cpus=-1):
        k_folds_classifier = KerasClassifier(build_fn=self.build_neural_network)

        if callbacks is not None:
            number_of_cpus = 1

        grid_search = GridSearchCV(estimator=k_folds_classifier,
                                   param_grid=parameters,
                                   cv=k_folds_number,
                                   n_jobs=number_of_cpus
                                  )

        self.classifier = grid_search.fit(data_training_set, class_training_set, verbose=2, callbacks=callbacks).best_estimator_.model
        self.grid_search_results = grid_search.cv_results_

        return grid_search.best_params_, grid_search.best_score_

    def predict_results(self, data_testing_set, class_testing_set):
        predicted_label = self.classifier.predict(data_testing_set)

        class_testing_set = [np.argmax(encoded) for encoded in class_testing_set]
        predicted_label   = [np.argmax(encoded) for encoded in predicted_label]

        accuracy = accuracy_score(predicted_label, class_testing_set)

        return confusion_matrix(predicted_label, class_testing_set), accuracy

    def save_model(self, folder):
        model_json = self.classifier.to_json()
        with open("../savedmodels/"+folder+"/model_struct.json", "w") as json_file:
            json_file.write(model_json)
        json_file.close()

        self.classifier.save_weights("../savedmodels"+folder+"/model_weights.h5")

    def calculate_hidden_layer_size(self, hidden_layer_size, input_layer_size, output_layer_size):
        return int((input_layer_size + output_layer_size) / 2) if hidden_layer_size is None else hidden_layer_size

    def get_model_accuracy_details(self):
        return self.accuracies.mean(), self.accuracies.std()
