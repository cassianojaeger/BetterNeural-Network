#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Created on Tue Jan 23 13:05:41 2018

@author: i857753
"""

import numpy as np
import pandas as pd
import pickle
from sklearn.preprocessing import Imputer
from sklearn.preprocessing import LabelEncoder, OneHotEncoder
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split
from keras.utils import np_utils


class Preparedataset:

    def __init__(self, fileName):
        self.read_data_set(fileName)
    
    def read_data_set(self, fileName):
        self.dataset = pd.read_csv(fileName)
        self.featureMatrix = self.dataset.iloc[:, :-1].values
        self.classesArray = self.dataset.iloc[:, -1].values
   
    def delete_unused_attributes(self, columns):
        self.featureMatrix = np.delete(self.featureMatrix, columns, axis=1)
    
    def handle_missing_data(self, strategy, missing_values, axis, columns):
        axis = 0 if axis == 'column' else 1
        imputer = Imputer(missing_values = missing_values, strategy = strategy,  axis = axis)
        missingDataColumns = self.featureMatrix[:, columns]
        self.featureMatrix[:, columns] = imputer.fit(missingDataColumns).transform(missingDataColumns)
        
    def handle_categorical_attr(self, categorical_columns, dummy_encoding_columns):
        labelencoder = LabelEncoder()
        # determine which column is going to transform from string to integers
        self.label_encoders_list = dict()
        for column in categorical_columns:
            self.label_encoders_list[column] = labelencoder.fit(self.featureMatrix[:, column])
            self.featureMatrix[:, column] = labelencoder.transform(self.featureMatrix[:, column])

        # determine which column is going to be transformed to dummy encoding
        self.featureOnehotencoder = OneHotEncoder(categorical_features = dummy_encoding_columns)

        self.featureOnehotencoder = self.featureOnehotencoder.fit(self.featureMatrix)
        self.featureMatrix = self.featureOnehotencoder.transform(self.featureMatrix).toarray()

        array_of_dummies = self.get_number_of_dummies(dummy_encoding_columns)
        self.handle_multicollinearity(array_of_dummies, dummy_encoding_columns)

    def encode_classes(self):
        labelencoder = LabelEncoder()
        self.classesArray = labelencoder.fit_transform(self.classesArray)
        self.classesArray = np_utils.to_categorical(self.classesArray)

    def generate_test_sets(self, test_size, random_seed):
        (feature_training_set,
        feature_testing_set,
        class_training_set,
        class_testing_set) = train_test_split(
                                self.featureMatrix,
                                self.classesArray,
                                test_size = test_size,
                                random_state = random_seed
        )
        return feature_training_set, feature_testing_set, class_training_set, class_testing_set

    def normalise_data(self, training_set, testing_set):
        self.standard_scaler = StandardScaler()
        training_set = self.standard_scaler.fit_transform(training_set)
        testing_set = self.standard_scaler.transform(testing_set)
        
        return training_set, testing_set

    def handle_multicollinearity(self, array_of_dummies, dummy_encoding_columns):
        self.delete_unused_attributes(0)
        stack = 0
        for val, skiped_columns in enumerate(array_of_dummies[:-1]):
            stack = skiped_columns - 1 + stack
            self.delete_unused_attributes(stack)

    def get_number_of_unique_values(self, column):
        return len(set(self.featureMatrix[:, column]))

    def get_number_of_dummies(self, dummy_encoding_columns):
        array_of_dummies = [(self.get_number_of_unique_values(dummy_column)) for dummy_column in dummy_encoding_columns]
        return array_of_dummies

    def save_scaler(self):
        scalerfile = '../savedmodels/liverpool/scaler.sav'
        pickle.dump(self.standard_scaler, open(scalerfile, 'wb'))

    def save_label_encoder(self):
        scalerfile = '../savedmodels/liverpool/label_encoders.sav'
        pickle.dump(self.label_encoders_list, open(scalerfile, 'wb'))

    def save_one_hot_encoders(self):
        scalerfile = '../savedmodels/liverpool/hot_encoders.sav'
        pickle.dump(self.featureOnehotencoder, open(scalerfile, 'wb'))