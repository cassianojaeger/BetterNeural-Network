import plotly
from plotly.graph_objs import Scatter, Layout
import numpy as np

class PlotResults:
    def __init__(self):
        pass

    def plot_grid_search(self, cv_results, grid_param_1, grid_param_2, name_param_1, name_param_2):
        # Get Test Scores Mean and std for each grid search
        scores_mean = cv_results['mean_test_score']
        scores_mean = np.array(scores_mean).reshape(len(grid_param_2), len(grid_param_1))

        scores_sd = cv_results['std_test_score']
        scores_sd = np.array(scores_sd).reshape(len(grid_param_2), len(grid_param_1))

        plotly.offline.plot({
            "data": [Scatter(x=grid_param_1,
                             y=np.ndarray.tolist(scores_mean[idx, :]),
                             name=name_param_2 + ' ' + str(param_value),
                             mode='lines+markers'
                            ) for idx, param_value in enumerate(grid_param_2)],
            "layout": Layout(title="Score Mean Graph - Iris Dataset",
                             xaxis=dict(title=name_param_1),
                             yaxis=dict(title=name_param_2)
                            )
        })