<a href="https://aimeos.org/">
    <img src="/images/ai.jpg" alt="Deep Learning" title="Aimeos" align="right" height="55" />
</a>

Enhanced Neural Network
======================
This is a side project aiming a better understanding of Keras/Tensorflow frameworks on Python.
My goal is to create a simple framework for ANN with simple plotting and saving model features.

## Table of content

- [Project Structure](#project-structure)
    - [Mains](#mains)
    - [Models](#models)
    - [Data Processing](#Dataprocessing)
    - [Callbacks](#callbacks)
    - [Saved Models](#saved-models)
    - [Data](#data)
- [Frameworks](#frameworks)
    - [Installing Dependencies](#installing-dependencies)
- [Creating Main Script](#frameworks)
    - [Preprocessing Data](#pre-processing-data)
    - [Save Model Callback](#save-model-callback)
    - [ANN Parameters](#ann-parameters)
    - [Training Model](#training-model)
    - [Visualizing Results](#visualizing-results)

## Project Structure 
My project is composed of 6 (six) folders, to make it more readable and scalable.

#### Mains 
Folder for the main scripts, there should be all your scripts for each dataset you want to use. Guide for building the main script can be found in the respective folder.

#### Models 
There should be all ML models used for the project, at the moment I'm only using simple ANN, but in the future I want to add more algorithms (**Random Forests, CNN, RNN**)

#### Dataprocessing 
This folder is responsible for handling data processing scripts. At the moment it provides **data normalization**, handlers for **missing** and **categorical** data, **multicollinearity** resolver, and **data splitting** strategy.

#### Callbacks 

All custom callbacks for Keras/Scikit methods are created on this folder. At the moment, only the ModelCheckPoint callback is implemented.

#### Saved Models
Here is where the checkpoint files generated for the ModelCheckPoint callback are save. Each dataset have its own folder for better organization.

#### Data ####
All CSV files that I use for model testing.

## Frameworks ##
In my project I make use of several python libraries. The main ones are Anaconda, Keras and Tensorflow. 
Below will be exposed all dependencies which need to be intalled in order to make everything works fine.

#### Installing Dependencies ####
The first dependency which should be installed is [Anaconda](https://conda.io/docs/user-guide/install/index.html). 
To do so, click in the previous link and download the installer for your OS.

[Tensorflow](https://www.tensorflow.org/), [Numpy](www.numpy.org), [Pandas](https://pandas.pydata.org/) and [Plotly](https://plot.ly/)
are already default packages from Anaconda, so there is no need to install them again.

After that, install the required dependencies using ``` conda install <package>``` command in command line.

- ``` conda install keras ```
- ``` conda install scikit-learn```

Or, you can just type ``` pip install -r requirements.txt ``` to install all dependencies needed for the project.

Future: Using venv to handle environments.

## Creating Main Script ##
More content soon...


