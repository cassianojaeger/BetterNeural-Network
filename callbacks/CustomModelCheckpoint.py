
from keras.callbacks import ModelCheckpoint

class CustomModelCheckpoint(ModelCheckpoint):
    def __init__(self, filepath, monitor='val_loss', verbose=0,
                 save_best_only=False, save_weights_only=True,
                 mode='auto', period=1):
        super(CustomModelCheckpoint, self).__init__(filepath, monitor, verbose, save_best_only,
                                                    save_weights_only, mode, period)

    def on_train_end(self, logs=None):
        super(CustomModelCheckpoint, self).on_train_end()

    def on_epoch_end(self, epoch, logs=None):
        super(CustomModelCheckpoint, self).on_epoch_end(epoch, logs)