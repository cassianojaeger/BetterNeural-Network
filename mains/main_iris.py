#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan 24 14:30:32 2018

@author: i857753
"""
import calendar
import time
from callbacks.CustomModelCheckpoint import CustomModelCheckpoint

from models.ann import NeuralNetwork
from dataprocessing.preprocessing import Preparedataset
from dataprocessing.plotresults import PlotResults

#import data
preprocessing = Preparedataset('../data/iris.csv')

#encode classes
preprocessing.encode_classes()

#create training and test data
feature_training_set, feature_testing_set, class_training_set, class_testing_set = preprocessing.generate_test_sets(0.2, 1)
feature_training_set, feature_testing_set = preprocessing.normalise_data(feature_training_set, feature_testing_set)

#alias for inside variables
featureMatrix = preprocessing.featureMatrix
classesArray = preprocessing.classesArray

#initializing neural network
ann = NeuralNetwork(featureMatrix[0].size, classesArray[0].size)

#creating logic for callback
# filepath="../savedmodels/iris/weights."+str(calendar.timegm(time.gmtime()))+".hdf5"
# checkpoint = CustomModelCheckpoint(filepath, monitor='acc', verbose=1, save_best_only=True, mode='max', period=100)
# callbacks_list = [checkpoint]
callbacks_list = None

#parameters for neural network
parameters= {'epochs': [1, 2, 3, 4],
             'batch_size': [5, 10, 15],
             'gradient_descent': ['adam'],
             'hidden_layer_size': [8],
             'hidden_layer_quantity': [1],
             'dropout': [0.1],
             'error_function': ['categorical_crossentropy'],
             'output_activation_func': ['softmax']
            }

#print best parameters and acc mean
best_parameters, mean = ann.run_parameter_tunning(feature_training_set, class_training_set, parameters, callbacks_list)
print("Média: "+str(mean), "\n Best Parameters: "+str(best_parameters))

#print confusion matrix
matrix, accuracy = ann.predict_results(feature_testing_set, class_testing_set)
print(matrix, "Accuracy: "+str(accuracy))

#plotting results
#param1: parameter initiating with last alphabetical order
#param2: parameter initiating with first alphabetical order
plt = PlotResults()
plt.plot_grid_search(ann.grid_search_results, parameters['epochs'], parameters['batch_size'], 'Epochs', 'Batch Size')