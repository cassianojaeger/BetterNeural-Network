#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan 24 14:30:32 2018

@author: i857753
"""
from models.ann import NeuralNetwork
from dataprocessing.preprocessing import Preparedataset
from dataprocessing.plotresults import PlotResults

preprocessing = Preparedataset('../data/Churn_Modelling.csv')
preprocessing.delete_unused_attributes([0, 1, 2])
preprocessing.handle_categorical_attr(categorical_columns=[1, 2], dummy_encoding_columns=[1])
#  We need to avoid dummy variables trap --> (the sum of the created columns is always 1 : Spain, France, Germany, Sum
#                                                                                           1       0       0       | 1
#                                                                                           0       1       0       | 1
#                                                                                           1       0       0       | 1
#                                                                                           0       0       1       | 1
#  Here we have Multicollinearity, in other words, some columns can be predicted based in other columns:
#                                                                                                   0       0       | 0
#                                                                                                   1       0       | 1
#                                                                                                   0       0       | 0
#                                                                                                   0       1       | 1
# It mens that Spain (First column with 1 value) can be predicted when France and Germany are 0)
# It has a correlation to 2ˆn. If we have 9 dummy columns from one attribute, we only need 4 columns --> 2ˆ4 > 9 > 2ˆ3
# In other words, 4 columns can code the 9 dummy columns
preprocessing.delete_unused_attributes([0])

feature_training_set, feature_testing_set, class_training_set, class_testing_set = preprocessing.generate_test_sets(0.2, 1)
feature_training_set, feature_testing_set = preprocessing.normalise_data(feature_training_set, feature_testing_set)

featureMatrix = preprocessing.featureMatrix
classesArray = preprocessing.classesArray


ann = NeuralNetwork(featureMatrix[0].size, 1)

#creating logic for callback
# filepath="../savedmodels/churn/weights."+str(calendar.timegm(time.gmtime()))+".hdf5"
# checkpoint = CustomModelCheckpoint(filepath, monitor='acc', verbose=1, save_best_only=True, mode='max', period=100)
# callbacks_list = [checkpoint]
callbacks_list = None

parameters= {'batch_size': [1000, 2000, 5000],
             'dropout': [0.1, 0.2],
             'epochs': [5],
             'gradient_descent': ['rmsprop'],
             'hidden_layer_size': [5],
             'hidden_layer_quantity': [2],
             'error_function': ['binary_crossentropy']
            }

best_parameters, mean = ann.run_parameter_tunning(feature_training_set, class_training_set, parameters, callbacks_list)
print("Média: "+str(mean), "\n Best Parameters: "+str(best_parameters))

matrix, accuracy = ann.predict_results(feature_testing_set, class_testing_set)
print(matrix, "Accuracy: "+str(accuracy))

#param1: parameter initiating with last alphabetical order
#param2: parameter initiating with first alphabetical order
plt = PlotResults()
plt.plot_grid_search(ann.grid_search_results, parameters['dropout'], parameters['batch_size'], 'Dropout', 'Batch Size')